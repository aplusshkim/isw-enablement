# Integration Design

![Integration Namespace](images/sevicing-order-integration.png){width="50%"}

---

## Swagger File Upload

### Download

[Party Lifecycle Management](https://education-dev.apps.openshift-01.knowis.cloud/swagger-ui/index.html?urls.primaryName=PARTYLIFE%20v1&configUrl=/v3/api-docs/swagger-config%3FdeploymentIdentifier%3Dk5-partylife)

[Payment Order](https://education-dev.apps.openshift-01.knowis.cloud/swagger-ui/index.html?urls.primaryName=PAYMORD%20v1&configUrl=/v3/api-docs/swagger-config%3FdeploymentIdentifier%3Dk5-paymord)

### Create Integration Namespace

![Payment Order](images/integration-namespace.png){width="50%"}

> ## Party Lifecycle Management : partylife
>
> | 이름   | 유형                       |
> | ------ | -------------------------- |
> | Prefix | partylife                  |
> | Label  | Party Lifecycle Management |

> ## Payment Order : paymord
>
> | 이름   | 유형          |
> | ------ | ------------- |
> | Prefix | paymord       |
> | Label  | Payment Order |

### Upload Swagger File

![Payment Order](images/integration-upload.png){width="50%"}

> ## partylife
>
> | 이름    | 유형         |
> | ------- | ------------ |
> | name    | partylife    |
> | Binding | Local Lookup |

> ## paymord
>
> | 이름    | 유형         |
> | ------- | ------------ |
> | name    | paymord      |
> | Binding | Local Lookup |

---

## Service - paymord

### Properties 생성

> | Local Identifier   | Type |
> | ------------------ | ---- |
> | accountNumber      | Text |
> | amount             | Text |
> | externalId         | Text |
> | externalSerive     | Text |
> | paymentType        | Text |
> | transactionId      | Text |
> | paymentOrderReulst | Text |

### PaymentOrder Service

![Payment Order](images/integration-service.png){width="50%"}

> | 이름             | 유형                 |
> | ---------------- | -------------------- |
> | Local Identifier | PaymentOrder         |
> | Label            | PaymentOrder Service |

> PaymentOrder_Input
>
> | Local Identifier | Type |
> | ---------------- | ---- |
> | accountNumber    | Text |
> | amount           | Text |
> | externalId       | Text |
> | externalSerive   | Text |
> | paymentType      | Text |

> PaymentOrder_Output
>
> | Local Identifier   | Type |
> | ------------------ | ---- |
> | transactionId      | Text |
> | paymentOrderReulst | Text |

## Service - paytylife

### Properties 생성

> | Local Identifier | Type |
> | ---------------- | ---- |
> | id               | Text |
> | password         | Text |
> | result           | Text |

### LoginExcute Service

> | 이름             | 유형                 |
> | ---------------- | -------------------- |
> | Local Identifier | LoginExcute          |
> | Label            | Login Excute Service |

> LoginExcute_Input
>
> | Local Identifier | Type |
> | ---------------- | ---- |
> | id               | Text |
> | password         | Text |

> LoginExcute_Output
>
> | Local Identifier | Type |
> | ---------------- | ---- |
> | result           | Text |

### RetrieveLogin Service

> | 이름             | 유형                   |
> | ---------------- | ---------------------- |
> | Local Identifier | RetrieveLogin          |
> | Label            | Retrieve Login Service |

> LoginExcute_Input
>
> | Local Identifier | Type |
> | ---------------- | ---- |
> | id               | Text |

> LoginExcute_Output
>
> | Local Identifier | Type |
> | ---------------- | ---- |
> | result           | Text |
